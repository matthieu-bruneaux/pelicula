### * Description

# Script to test the Movie class defined in "movieR6.R"

### * Setup

source("movieR6.R")

### * Basic animation

m <- Movie$new()
m$setOutputDir("test", delete.frames = TRUE)
m$setFPS(24)
for (t in seq(0, 4*pi, length.out = 128)) {
    m$nextFrame()
    plot(0, type = "n", xlim = c(1, 10), ylim = c(-1, 11))
    xAt <- 1:10
    lines(xAt, xAt + sin(xAt * t), lwd = 2)
    points(xAt + sin(xAt * t), pch = 21, bg = viridis::viridis(10), cex = 2)
}
m$renderMovie("basic-animation.mp4")

### * Create strips

m <- Movie$new()
m$setFPS(24)

m$addStrip("anim1", firstFrame = 4, nFrames = 4)
m$addStrip("anim2", startTime = 3, duration = 2)
m$strips
