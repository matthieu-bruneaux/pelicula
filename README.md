[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/pelicula/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/pelicula/commits/master)
[![Coverage report](https://gitlab.com/matthieu-bruneaux/pelicula/badges/master/coverage.svg)](https://matthieu-bruneaux.gitlab.io/pelicula/coverage/coverage.html)
[![R_CMD_CHECK](https://matthieu-bruneaux.gitlab.io/pelicula/r-cmd-check-badge.svg)](https://matthieu-bruneaux.gitlab.io/pelicula/R_CMD_CHECK.txt)
[![Lifecycle Status](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/)

## pelicula

`pelicula` is a package to render individual frames of an animation with fine
control and combine them into a movie file.

## Visit the documentation!

The [documentation pages](https://matthieu-bruneaux.gitlab.io/pelicula/) will show
you what this package can do, how to install it and how to get started.

## Related tools

- `animation` package
- `av` package
- `gganimate` package
- `tweenr` package
- `magick` package
